<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "assets";
    protected $guarded = [];

    public function room()
    {
        return $this->hasMany(RoomModel::class, 'asset_id', 'id');
    }

    public function image()
    {
        return $this->hasMany(ImageAssetModel::class, 'asset_id', 'id');
    }
}
