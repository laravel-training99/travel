<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageAssetModel extends Model
{
    use HasFactory;
    protected $table = "image_assets";
    protected $guarded = [];
}
