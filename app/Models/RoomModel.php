<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "rooms";
    protected $guarded = [];

    public function asset()
    {
        return $this->belongsTo(AssetModel::class, 'asset_id', 'id');
    }

    public function image()
    {
        return $this->hasMany(ImageAssetModel::class, 'asset_id', 'id');
    }

    public function bookings() {
        return $this->hasMany(BookingModel::class, 'room_id', 'id');
    }
}
