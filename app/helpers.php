<?php
define('ACTIVE', 2);
define('INACTIVE', 1);
define('ADMIN', 3);
define('CLIENT', 1);
define('PARTNER', 2);
define('PENDING', 1);
define('PAID', 2);
define('DONE', 3);
define('VNP_SUCCESS', 00);
define('VNP_UNPAY', 24);
function preview($text)
{
    $preview = strlen($text) > 35 ? substr($text,0,35)."..." : $text;

    return $preview;
}

function uploadFile ($file) {
    $fileName = time().'.'.$file->extension();

    $file->move(public_path('/images'), $fileName);

    return "/images/" . $fileName;
}

function getAge($date)
{
    $now = explode("-", date('Y-m-d'));
    $dob = explode("-", $date);
    $dif = $now[0] - $dob[0];
    if ($dob[1] > $now[1]) {
        $dif -= 1;
    } elseif ($dob[1] == $now[1]) {
        if ($dob[2] > $now[2]) {
            $dif -= 1;
        }
        elseif ($dob[2] == $now[2]) {
            $dif = $dif . " (Sinh nhật!)";
        };
    };
    return $dif;
}

function checkRoom($id)
{
    $room = \App\Models\RoomModel::where('asset_id', $id)->first();
    if ($room){
        return false;
    }
    return true;
}

function checkBooking($id)
{
    $booking = \App\Models\BookingModel::where('room_id', $id)->first();
    if ($booking){
        return false;
    }
    return true;
}

function timeAgo($time_ago)
{
    $time_ago = strtotime($time_ago);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed ;
    $minutes    = round($time_elapsed / 60 );
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400 );
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640 );
    $years      = round($time_elapsed / 31207680 );
    // Seconds
    if($seconds <= 60) {
        return "just now";
    } else if($minutes <=60) {
        if($minutes==1) {
            return "one minute ago";
        }
        else {
            return "$minutes minutes ago";
        }
    }
    //Hours
    else if($hours <=24) {
        if($hours==1) {
            return "an hour ago";
        }
        else {
            return "$hours hrs ago";
        }
    } else if($days <= 7) {
        if($days==1) {
            return "yesterday";
        } else {
            return "$days days ago";
        }
    }
    //Weeks
    else if($weeks <= 4.3) {
        if($weeks==1){
            return "a week ago";
        } else {
            return "$weeks weeks ago";
        }
    }
    //Months
    else if($months <=12){
        if($months==1) {
            return "a month ago";
        } else {
            return "$months months ago";
        }
    }
    //Years
    else {
        if($years==1) {
            return "one year ago";
        } else {
            return "$years years ago";
        }
    }
}
