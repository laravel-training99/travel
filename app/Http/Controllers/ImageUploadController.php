<?php

namespace App\Http\Controllers;

use App\Models\ImageAssetModel;
use Illuminate\Http\Request;

class ImageUploadController extends Controller
{
    protected $image;
    public function __construct(ImageAssetModel $image)
    {
        $this->image = $image;
    }
    public function fileStore(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images'),$imageName);
        $imageUpload = $this->image->create([
            'image_path' => $imageName,
            'asset_id' => '1',
            'room_id' => '1',
        ]);
        return response()->json($imageUpload->id);
    }

    public function fileDestroy(Request $request)
    {
        $filename =  $request->get('filename');
        $image = $this->image->where('image_path',$filename)->first();
        $image->delete();
        $path=public_path().'/images/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return response()->json($image->id);
    }

    public function imageUpdateData(Request $request)
    {
        $image = $this->image->where('asset_id', $request->id)->get();
        return response()->json($image);
    }
}
