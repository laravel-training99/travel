<?php

namespace App\Http\Controllers;

use App\Models\BookingModel;
use App\Models\RoomModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{
    protected $booking;
    protected $room;
    public function __construct(BookingModel $booking, RoomModel $room)
    {
        $this->booking = $booking;
        $this->room = $room;
    }
    public function dashboard()
    {
        $newBookings = $this->booking->where('status', PENDING)->with('room')
            ->whereHas('room', function ($query){
                $query->where('user_id', Auth::id());
            })
            ->get();
        $checkinBookings = $this->booking->where('start_date', 'like', '%'. Carbon::today()->toDateString() .'%')->with('room')
            ->whereHas('room', function ($query){
                $query->where('user_id', Auth::id());
            })
            ->get();
        $checkoutBookings = $this->booking->where('end_date', 'like', '%'. Carbon::today()->toDateString() .'%')->with('room')
            ->whereHas('room', function ($query){
                $query->where('user_id', Auth::id());
            })
            ->get();
        return view('main.partner.dashboard', compact('newBookings', 'checkinBookings', 'checkoutBookings'));
    }
}
