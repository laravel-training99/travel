<?php

namespace App\Http\Controllers;

use App\Models\BookingModel;
use App\Models\PaymentModel;
use App\Models\RoomModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    protected $booking;
    protected $user;
    protected $room;
    protected $transaction;
    public function __construct(User $user, RoomModel $room, BookingModel $booking, PaymentModel $transaction )
    {
        $this->user = $user;
        $this->room = $room;
        $this->booking = $booking;
        $this->transaction = $transaction;
    }
    public function indexClient()
    {
        $title = 'Transaction';
        $keyword = request()->get('keyword');
        $transactions = $this->transaction->where('user_id', Auth::id())
            ->where(function ($query) use($keyword) {
                $query->where('code', 'like', '%' . $keyword . '%')
                    ->orWhere('booking_code', 'like', '%' . $keyword . '%');
            })->paginate(10);
        return view('clients.transaction', compact('title', 'transactions'));
    }
}
