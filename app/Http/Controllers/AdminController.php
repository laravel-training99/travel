<?php

namespace App\Http\Controllers;

use App\Models\AssetModel;
use App\Models\BookingModel;
use App\Models\LocationModel;
use App\Models\PaymentModel;
use App\Models\RoomModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    protected $user;
    protected $location;
    protected $asset;
    protected $room;
    protected $booking;
    protected $transaction;
    public function __construct(User $user, LocationModel $location, AssetModel $asset, RoomModel $room, BookingModel $booking, PaymentModel $transaction)
    {
        $this->user = $user;
        $this->location = $location;
        $this->asset = $asset;
        $this->room = $room;
        $this->booking = $booking;
        $this->transaction = $transaction;
    }
    public function clients()
    {
        $title = 'List Client';
        $oldKeyword = request()->get('keyword');
        $oldStatus = request()->get('status');
        $users = $this->user->where('role', CLIENT)
            ->where('name', 'like', '%' . request()->get('keyword') .'%')
            ->orderBy('id')
            ->paginate(5);
        if (request()->get('status')){
            $users = $this->user->where('role', CLIENT)
                ->where('name', 'like', '%' . request()->get('keyword') .'%')
                ->where('status', request()->get('status'))
                ->orderBy('id')
                ->paginate(5);
        }
        return view('main.admin.client.list', compact('users', 'title', 'oldKeyword', 'oldStatus'));
    }

    public function activeOrInactiveUser($id)
    {
        $user = $this->user->find($id);
        $user->update([
            "status" => $user->status == ACTIVE ? INACTIVE : ACTIVE
        ]);
        return redirect()->back();
    }

    public function partners()
    {
        $title = 'List Partner';
        $users = $this->user->where('role', PARTNER)->orderBy('id')->paginate(5);
        return view('main.admin.partner.list', compact('users', 'title'));
    }

    public function locations()
    {
        $locations = $this->location->with('province')->orderBy('id')->paginate(5);
        return view('main.admin.location.list', compact('locations'));
    }

    public function deleteLocation($id)
    {
        if ($this->location->find($id)->delete()){
            return response()->json(['status' => true, 'message' => 'Delete success']);
        }
        return response()->json(['status' => false, 'message' => 'Delete error']);
    }

    public function assets()
    {
        $title = 'List Assets';
        $assets = $this->asset->orderBy('id')->paginate(5);
        return view('main.admin.asset.list', compact('assets', 'title'));
    }

    public function blockAsset($id)
    {
        try {
            $this->asset->find($id)->delete();
        } catch (\Throwable $e) {
            return back()->with('message', $e)->with('status', false);
        }

        return redirect()->route('rooms.index')->with('message', 'remove block success')->with('status', true);
    }

    public function assetBlock()
    {
        $title = 'List asset block';
        $assets = $this->asset->onlyTrashed()->orderBy('id')->paginate(5);
        return view('main.admin.asset.list_block', compact('assets', 'title'));
    }

    public function restoreAsset($id)
    {
        $this->asset->withTrashed()
            ->where('id', $id)
            ->restore();
        return redirect()->route('admin.asset.list');
    }

    public function rooms()
    {
        $title = 'List room';
        $rooms = $this->room->with('asset')->orderBy('id')->paginate(5);
        return view('main.admin.rooms.list', compact('rooms', 'title'));
    }

    public function roomBlock()
    {
        $title = 'List room block';
        $rooms = $this->room->onlyTrashed()->with('asset')->orderBy('id')->paginate(5);
        return view('main.admin.rooms.list_block', compact('rooms', 'title'));
    }

    public function restoreRoom($id)
    {
        $this->room->withTrashed()
            ->where('id', $id)
            ->restore();
        return redirect()->route('admin.room.list');
    }

    public function blockRoom($id)
    {
        try {
            $this->room->find($id)->delete();
        } catch (\Throwable $e) {
            return back()->with('message', $e)->with('status', false);
        }

        return redirect()->route('rooms.index')->with('message', 'remove block success')->with('status', true);
    }

    public function dashboard()
    {
        $title = 'Dasboard';
        $userCount = $this->user->get();
        $newBookings = $this->booking->where('status', PENDING)->with('room')->get();
        $checkinBookings = $this->booking->where('start_date', 'like', '%'. Carbon::today()->toDateString() .'%')->get();
        $checkoutBookings = $this->booking->where('end_date', 'like', '%'. Carbon::today()->toDateString() .'%')->get();
        return view('main.admin.dashboard', compact('title', 'newBookings', 'checkinBookings', 'checkoutBookings', 'userCount'));
    }

    public function bookings()
    {
        $title = "List booking";
        $bookings = $this->booking->with('room', 'user', 'image')->paginate(5);
        return view('main.admin.bookings.index', compact('bookings', 'title'));
    }

    public function transactions()
    {
        $title = 'Transactions';
        $transactions = $this->transaction->with('user')->paginate(1);
        return view('main.admin.transactions.list', compact('title', 'transactions'));
    }
}
