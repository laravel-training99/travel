<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zxx">
<!-- Mirrored from htmldesigntemplates.com/html/travelin/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Sep 2022 07:00:48 GMT -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Travelin - @yield('title')</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('client')}}/images/favicon.png" />

    <link href="{{asset('client/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('client/css/style.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('client/css/plugin.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
    <link rel="stylesheet" href="{{asset('client/fonts/line-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <style>
        .spinner-border{
            width: 1rem !important;
            height: 1rem !important;
        }
        .spin-loading-block{
            display: none !important;
        }
    </style>
    @yield('styles')
</head>
<body>
<div id="preloader">
    <div id="status"></div>
</div>
@include('clients.components.header')
@yield('contents')
@include('clients.components.footer')
<script data-cfasync="false" src="https://htmldesigntemplates.com/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src="{{asset('client/js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('client/js/bootstrap.min.js')}}"></script>
<script src="{{asset('client/js/particles.js')}}"></script>
<script src="{{asset('client/js/particlerun.js')}}"></script>
<script src="{{asset('client/js/plugin.js')}}"></script>
<script src="{{asset('client/js/main.js')}}"></script>
<script src="{{asset('client/js/custom-swiper.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
    (function () {
        var js =
            "window['__CF$cv$params']={r:'74655461cd4b8938',m:'dqkRly.NbUmYNfjhgnR6q2s.I.QbhzsYaCXGr2P2MGg-1662447647-0-AfCWLMNZ8Gh2Zpxxt89lXDr3VVRoECQNkdBvcjlLM5IMMdxLrScKbXr/4N5dGkgr7IjI0WMCksXpmRam+DRh03RQ8qXwYYczsyiNVV1+nWxr59fcCR23zsdihxZPQGwgUgyTMZ65pPQRbWqDWAhOEa8=',s:[0x0f977dd05c,0xd099e14357],u:'/cdn-cgi/challenge-platform/h/b'};var now=Date.now()/1000,offset=14400,ts=''+(Math.floor(now)-Math.floor(now%offset)),_cpo=document.createElement('script');_cpo.nonce='',_cpo.src='https://htmldesigntemplates.com/cdn-cgi/challenge-platform/h/b/scripts/alpha/invisible.js?ts='+ts,document.getElementsByTagName('head')[0].appendChild(_cpo);";
        var _0xh = document.createElement("iframe");
        _0xh.height = 1;
        _0xh.width = 1;
        _0xh.style.position = "absolute";
        _0xh.style.top = 0;
        _0xh.style.left = 0;
        _0xh.style.border = "none";
        _0xh.style.visibility = "hidden";
        document.body.appendChild(_0xh);
        function handler() {
            var _0xi = _0xh.contentDocument || _0xh.contentWindow.document;
            if (_0xi) {
                var _0xj = _0xi.createElement("script");
                _0xj.nonce = "";
                _0xj.innerHTML = js;
                _0xi.getElementsByTagName("head")[0].appendChild(_0xj);
            }
        }
        if (document.readyState !== "loading") {
            handler();
        } else if (window.addEventListener) {
            document.addEventListener("DOMContentLoaded", handler);
        } else {
            var prev = document.onreadystatechange || function () {};
            document.onreadystatechange = function (e) {
                prev(e);
                if (document.readyState !== "loading") {
                    document.onreadystatechange = prev;
                    handler();
                }
            };
        }
    })();
</script>
@include('clients.components.account_model')

@yield('scripts')
</body>

<!-- Mirrored from htmldesigntemplates.com/html/travelin/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Sep 2022 07:02:42 GMT -->
</html>
