@extends('layouts.main')
@section('title', $title)
@section('contents')
    <div class="row mt-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="tab-content">
                        <div class="tab-pane active show" >
                            <div class="table-responsive">
                                <table class="table card-table display mb-4 shadow-hover default-table table-responsive-lg dataTable no-footer" id="guestTable-all">
                                    <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Start date</th>
                                        <th>End date</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($bookings as $value)
                                        <tr>
                                            <td>
                                                <div class="room-list-bx d-flex align-items-center">
                                                    <div class="concierge-bx d-flex align-items-center">
                                                        <img class="me-3 rounded" src="{{$value->user->image}}" style="object-fit: cover;width: 40px;height: 40px;" alt="">
                                                        <div>
                                                            <h5 class="fs-16 mb-0 text-nowrap"><a class="text-black" href="javascript:void(0);">{{$value->user->name}}</a></h5>
                                                            <span class="text-primary fs-14">{{$value->user->email}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <p class="font-w600">
                                                        {{$value->start_date}}
                                                    </p>
                                                </div>
                                            </td>
                                             <td>
                                                <div>
                                                    <p class="font-w600">
                                                        {{$value->end_date}}
                                                    </p>
                                                </div>
                                            </td>
                                            <td>
                                                {{number_format($value->total_money)}} đ
                                            </td>
                                            <td>
                                                @if($value->status == PENDING)
                                                    <span class="text-warning">Pending</span>
                                                @elseif($value->status == PAID)
                                                    <span class="text-success">Paid</span>
                                                @else
                                                    <span class="text-purple">Close</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-between">
                                    <div class="dataTables_info" id="guestTable-all_info" role="status" aria-live="polite"></div>
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        <nav>
                                            @include('main.component.pagination',['datas' => $bookings])
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

