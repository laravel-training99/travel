@extends('layouts.main')
@section('contents')
    <div class="row mt-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="AllEmployee">
                            <div class="table-responsive">
                                <table class="table card-table display mb-4 shadow-hover default-table table-responsive-lg dataTable no-footer" id="guestTable-all">
                                    <thead>
                                    <tr>
                                        <th>Đối tác</th>
                                        <th>Địa chỉ</th>
                                        <th>Liên hệ</th>
                                        <th>Trạng thái</th>
                                        <th class="bg-none"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $value)
                                    <tr>
                                        <td>
                                            <div class="concierge-list-bx d-flex align-items-center">
                                                <img class="me-3 rounded" src="{{$value->image}}" alt="">
                                                <div>
                                                    <h5 class="fs-16 mb-0 text-nowrap"><a class="text-black" href="javascript:void(0);">{{$value->user_name}}</a></h5>
                                                    <span class=" text-secondary fs-14 d-block">{{$value->name}}</span>
                                                    <span class=" fs-14 text-nowrap">Đăng ký {{timeAgo($value->created_at)}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="job-desk3">
                                            <p class="mb-0">{{$value->address}}</p>
                                        </td>
                                        <td>
                                            <div>
                                                <span class="font-w600 text-nowrap"><i class="fas fa-phone-alt me-2 "></i>{{$value->phone}}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <span  class="font-w600 {{$value->status == ACTIVE ? "text-success" : "text-danger"}}">
                                                {{$value->status == ACTIVE ? "ACTIVE" : "INACTIVE"}}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{route('admin.partner.edit', $value->id)}}" class="btn btn-sm {{$value->status == ACTIVE ? "btn-danger" : "btn-success"}}">
                                                {{$value->status == ACTIVE ? "INACTIVE" : "ACTIVE"}}
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-between">
                                    <div class="dataTables_info" id="guestTable-all_info" role="status" aria-live="polite"></div>
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        <nav>
                                            @include('main.component.pagination',['datas' => $users])
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

