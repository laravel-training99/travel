@extends('layouts.main')
@section('title', $title)
@section('styles')
    <link rel="stylesheet" href="{{asset('main')}}/vendor/toastr/css/toastr.min.css">
@endsection
@section('contents')
    <div class="d-flex justify-content-between align-items-center flex-wrap">
        <div class="card-action coin-tabs mb-3">

        </div>
        <div class="d-flex align-items-center mb-3">
            <a href="{{route('admin.room.block.list')}}" class="btn btn-primary">Room block <i class="fa fa-exclamation-circle"></i></a>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="tab-content">
                        <div class="tab-pane active show" >
                            <div class="table-responsive">
                                <table class="table card-table display mb-4 shadow-hover default-table table-responsive-lg dataTable no-footer" id="guestTable-all">
                                    <thead>
                                    <tr>
                                        <th>Room</th>
                                        <th>Description</th>
                                        <th>Rate</th>
                                        <th>Created at</th>
                                        <th class="bg-none">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($rooms as $value)
                                        <tr>
                                            <td>
                                                <div class="room-list-bx d-flex align-items-center">
                                                    <img class="me-3 rounded" style="object-fit: cover" src="/images/{{$value->image[0]->image_path}}" alt="">
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <p class="font-w600">
                                                        {{preview($value->description)}}
                                                    </p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="">
                                                    <span class="font-w500 text-nowrap">{{number_format($value->price)}} VND<small class="fs-14">/night</small></span>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="font-w600 text-nowrap">{{timeAgo($value->created_at)}}</span>
                                            </td>
                                            <td class="text-nowrap">
                                                <form action="{{route('admin.room.block', $value->id)}}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    @if(checkBooking($value->id))
                                                        <button type="submit" class="btn btn-icon btn-sm btn-danger">
                                                            <i class="fa fa-exclamation-circle"></i>
                                                        </button>
                                                    @endif
                                                    <button class="btn btn-sm btn-success" type="button" data-bs-toggle="modal" data-bs-target="#bd-room-{{$value->id}}"><i class="fa fa-eye"></i></button>
                                                </form>
                                            </td>
                                            <div class="modal fade bd-example-modal-lg" tabindex="-1" id="bd-room-{{$value->id}}" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" style="max-width: 1000px">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Room info</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal">
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-xl-12">
                                                                    <div class="card overflow-hidden">
                                                                        <div class="row m-0">
                                                                            <div class="col-xl-6 p-0">
                                                                                <div class="card-body">

                                                                                    <div class="d-flex flex-wrap">
                                                                                        <div class="check-status">
                                                                                            <span class="d-block mb-2">Room Info</span>
                                                                                            <h4 class="font-w500 fs-24">{{$value->asset->name}}</h4>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <span class="d-block mb-2 text-black">Price</span>
                                                                                            <span class="font-w500 fs-24 text-black">{{number_format($value->price)}} VND<small class="fs-14 ms-2 text-secondary">/night</small></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <p class="mt-2">
                                                                                        {{$value->description}}
                                                                                    </p>
                                                                                    <div class="facilities">
                                                                                        <div class="mb-3 ">
                                                                                            <span class="d-block mb-3">Facilities</span>
                                                                                            <a href="javascript:void(0);" class="btn btn-secondary light btn-lg">
                                                                                                <svg class="me-2" xmlns="http://www.w3.org/2000/svg" width="28" height="20" viewBox="0 0 28 20">
                                                                                                    <g>
                                                                                                        <path  d="M27,14V7a1,1,0,0,0-1-1H6A1,1,0,0,0,5,7v7a3,3,0,0,0-3,3v8a1,1,0,0,0,2,0V24H28v1a1,1,0,0,0,2,0V17A3,3,0,0,0,27,14ZM7,8H25v6H24V12a2,2,0,0,0-2-2H19a2,2,0,0,0-2,2v2H15V12a2,2,0,0,0-2-2H10a2,2,0,0,0-2,2v2H7Zm12,6V12h3v2Zm-9,0V12h3v2ZM4,17a1,1,0,0,1,1-1H27a1,1,0,0,1,1,1v5H4Z" transform="translate(-2 -6)" fill="#135846"/>
                                                                                                    </g>
                                                                                                </svg>
                                                                                                {{$value->bed}} Bed</a>
                                                                                            <a href="javascript:void(0);" class="btn btn-secondary light">
                                                                                                <i class="me-2 la la-water"></i> {{$value->bathroom}} Bathroom
                                                                                            </a>
                                                                                            <a href="javascript:void(0);" class="btn btn-secondary light">
                                                                                                <i class="me-2 la la-television"></i> {{$value->television}} Television
                                                                                            </a>
                                                                                            <a href="javascript:void(0);" class="btn btn-secondary light">
                                                                                                <i class="icon-Fridge" style="font-family: 'icomoon';"></i> {{$value->fridge}} Fridge
                                                                                            </a>
                                                                                            <a href="javascript:void(0);" class="btn btn-secondary light">
                                                                                                <i class="icon-people"></i> Up to {{$value->amount_of_people}} people
                                                                                            </a>
                                                                                            <a href="javascript:void(0);" class="btn btn-secondary light">
                                                                                                <svg version="1.1" class="me-1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                                     viewBox="0 0 60 60" style="width: 16px;height: 16px;" xml:space="preserve">
<g>
    <path d="M18.35,20.805c0.195,0.195,0.451,0.293,0.707,0.293c0.256,0,0.512-0.098,0.707-0.293c0.391-0.391,0.391-1.023,0-1.414
		c-1.015-1.016-1.015-2.668,0-3.684c0.87-0.87,1.35-2.026,1.35-3.256s-0.479-2.386-1.35-3.256c-0.391-0.391-1.023-0.391-1.414,0
		s-0.391,1.023,0,1.414c0.492,0.492,0.764,1.146,0.764,1.842s-0.271,1.35-0.764,1.842C16.555,16.088,16.555,19.01,18.35,20.805z"/>
    <path d="M40.35,20.805c0.195,0.195,0.451,0.293,0.707,0.293c0.256,0,0.512-0.098,0.707-0.293c0.391-0.391,0.391-1.023,0-1.414
		c-1.015-1.016-1.015-2.668,0-3.684c0.87-0.87,1.35-2.026,1.35-3.256s-0.479-2.386-1.35-3.256c-0.391-0.391-1.023-0.391-1.414,0
		s-0.391,1.023,0,1.414c0.492,0.492,0.764,1.146,0.764,1.842s-0.271,1.35-0.764,1.842C38.555,16.088,38.555,19.01,40.35,20.805z"/>
    <path d="M29.35,14.805c0.195,0.195,0.451,0.293,0.707,0.293c0.256,0,0.512-0.098,0.707-0.293c0.391-0.391,0.391-1.023,0-1.414
		c-1.015-1.016-1.015-2.668,0-3.684c0.87-0.87,1.35-2.026,1.35-3.256s-0.479-2.386-1.35-3.256c-0.391-0.391-1.023-0.391-1.414,0
		s-0.391,1.023,0,1.414c0.492,0.492,0.764,1.146,0.764,1.842s-0.271,1.35-0.764,1.842C27.555,10.088,27.555,13.01,29.35,14.805z"/>
    <path d="M55.624,43.721C53.812,33.08,45.517,24.625,34.957,22.577c0.017-0.16,0.043-0.321,0.043-0.48c0-2.757-2.243-5-5-5
		s-5,2.243-5,5c0,0.159,0.025,0.32,0.043,0.48C14.483,24.625,6.188,33.08,4.376,43.721C2.286,44.904,0,46.645,0,48.598
		c0,5.085,15.512,8.5,30,8.5s30-3.415,30-8.5C60,46.645,57.714,44.904,55.624,43.721z M27.006,22.27
		C27.002,22.212,27,22.154,27,22.098c0-1.654,1.346-3,3-3s3,1.346,3,3c0,0.057-0.002,0.114-0.006,0.172
		c-0.047-0.005-0.094-0.007-0.14-0.012c-0.344-0.038-0.69-0.065-1.038-0.089c-0.128-0.009-0.255-0.022-0.383-0.029
		c-0.474-0.026-0.951-0.041-1.432-0.041s-0.958,0.015-1.432,0.041c-0.128,0.007-0.255,0.02-0.383,0.029
		c-0.348,0.024-0.694,0.052-1.038,0.089C27.1,22.263,27.053,22.264,27.006,22.27z M25.126,26.635
		c1.582-0.356,3.217-0.537,4.86-0.537c0.004,0,0.009,0,0.014,0c0.552,0,1,0.448,1,1.001c0,0.552-0.448,0.999-1,0.999h0
		c-0.004,0-0.009,0-0.013,0c-1.496,0-2.982,0.164-4.421,0.488c-0.074,0.017-0.148,0.024-0.221,0.024c-0.457,0-0.87-0.315-0.975-0.78
		C24.249,27.291,24.587,26.756,25.126,26.635z M19.15,28.997c0.476-0.281,1.088-0.124,1.37,0.351
		c0.282,0.476,0.125,1.089-0.351,1.37c-4.713,2.792-8.147,7.861-9.186,13.56c-0.088,0.482-0.509,0.82-0.983,0.82
		c-0.06,0-0.12-0.005-0.18-0.017c-0.543-0.099-0.904-0.619-0.805-1.163C10.158,37.658,13.947,32.08,19.15,28.997z M30,55.098
		c-17.096,0-28-4.269-28-6.5c0-0.383,0.474-1.227,2.064-2.328c-0.004,0.057-0.002,0.113-0.006,0.17C4.024,46.988,4,47.54,4,48.098
		v0.788l0.767,0.185c8.254,1.981,16.744,2.985,25.233,2.985s16.979-1.004,25.233-2.985L56,48.886v-0.788
		c0-0.558-0.024-1.109-0.058-1.658c-0.004-0.057-0.002-0.113-0.006-0.17C57.526,47.371,58,48.215,58,48.598
		C58,50.829,47.096,55.098,30,55.098z"/>
</g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
                                                                                                    <g>
                                                                                                    </g>
</svg>
                                                                                                {{config('common')['breakfast'][$value->breakfast]['name']}}
                                                                                            </a>
                                                                                            <a href="javascript:void(0);" class="btn btn-secondary light btn-lg">
                                                                                                <svg class="me-2" xmlns="http://www.w3.org/2000/svg" width="20" height="23.512" viewBox="0 0 20 23.512">
                                                                                                    <g id="_010-security" data-name="010-security" transform="translate(-310.326 -159.324)">
                                                                                                        <path id="Path_1958" data-name="Path 1958" d="M330.326,165.226a2.952,2.952,0,0,0-1.71-2.8l-7.5-2.951a2.139,2.139,0,0,0-1.581,0l-7.5,2.951a2.951,2.951,0,0,0-1.709,2.8v5.318a10.445,10.445,0,0,0,4.372,8.772l5.142,3.372a.871.871,0,0,0,.971,0l5.143-3.372a10.448,10.448,0,0,0,4.372-8.772Zm-2,0a.591.591,0,0,0-.342-.561l-7.5-2.951a.432.432,0,0,0-.317,0l-7.5,2.951a.59.59,0,0,0-.341.561v5.318a7.985,7.985,0,0,0,3.343,6.707l4.657,3.054,4.656-3.054a7.986,7.986,0,0,0,3.344-6.707Zm-8.657,7.273,4.949-5.843a.9.9,0,0,1,1.415,0,1.338,1.338,0,0,1,0,1.67L320.376,175a.9.9,0,0,1-1.414,0l-2.829-3.338a1.337,1.337,0,0,1,0-1.669.9.9,0,0,1,1.414,0Z" transform="translate(0 0)" fill="#135846" fill-rule="evenodd"/>
                                                                                                    </g>
                                                                                                </svg>
                                                                                                24 Hours Guard</a>
                                                                                            <a href="javascript:void(0);" class="btn btn-secondary light btn-lg">
                                                                                                <svg class="me-2" xmlns="http://www.w3.org/2000/svg" width="20" height="15.75" viewBox="0 0 20 15.75">
                                                                                                    <g id="internet" transform="translate(0 -2.15)">
                                                                                                        <g id="Group_22" data-name="Group 22">
                                                                                                            <path id="Path_1969" data-name="Path 1969" d="M18.3,7.6a11.709,11.709,0,0,0-16.6,0,.967.967,0,0,1-1.4,0,.967.967,0,0,1,0-1.4,13.641,13.641,0,0,1,19.4,0,.99.99,0,0,1-1.4,1.4Z" fill="#135846"/>
                                                                                                        </g>
                                                                                                        <g id="Group_23" data-name="Group 23">
                                                                                                            <path id="Path_1970" data-name="Path 1970" d="M15.4,10.4a7.667,7.667,0,0,0-10.7,0A.99.99,0,0,1,3.3,9,9.418,9.418,0,0,1,16.8,9a.99.99,0,0,1-1.4,1.4Z" fill="#135846"/>
                                                                                                        </g>
                                                                                                        <g id="Group_24" data-name="Group 24">
                                                                                                            <path id="Path_1971" data-name="Path 1971" d="M12.6,13.4a3.383,3.383,0,0,0-4.9,0,.967.967,0,0,1-1.4,0,1.087,1.087,0,0,1,0-1.5,5.159,5.159,0,0,1,7.7,0,1.088,1.088,0,0,1,0,1.5A.967.967,0,0,1,12.6,13.4Z" fill="#135846"/>
                                                                                                        </g>
                                                                                                        <circle id="Ellipse_10" data-name="Ellipse 10" cx="1.9" cy="1.9" r="1.9" transform="translate(8.2 14.1)" fill="#135846"/>
                                                                                                    </g>
                                                                                                </svg>
                                                                                                Free Wifi</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-6 p-0">
                                                                                <div class="guest-carousel owl-carousel owl-carousel owl-loaded owl-drag owl-dot">
                                                                                    @foreach($value->image as $item)
                                                                                        <div class="item">
                                                                                            <div class="rooms">
                                                                                                <img src="/images/{{$item->image_path}}" alt="">
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-between">
                                    <div class="dataTables_info" id="guestTable-all_info" role="status" aria-live="polite"></div>
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        <nav>
                                            @include('main.component.pagination',['datas' => $rooms])
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('main')}}/vendor/toastr/js/toastr.min.js"></script>
    <script src="{{asset('main')}}/js/plugins-init/toastr-init.js"></script>
    <script>
        let message = '{{ session('message') }}'
        let status = '{{ session('status') }}'
        if(message) {
            if(status){
                toastr.success(message)
            }else {
                toastr.error(message)
            }
        }
        function TravlCarousel()
        {
            jQuery('.guest-carousel').owlCarousel({
                loop:false,
                margin:15,
                nav:true,
                autoplaySpeed: 3000,
                navSpeed: 3000,
                paginationSpeed: 3000,
                slideSpeed: 3000,
                smartSpeed: 3000,
                autoplay: false,
                animateOut: 'fadeOut',
                dots:true,
                navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
                responsive:{
                    0:{
                        items:1
                    },

                    480:{
                        items:1
                    },

                    767:{
                        items:1
                    },
                    1750:{
                        items:1
                    },
                    1920:{
                        items:1
                    },
                }
            })
        }

        jQuery(window).on('load',function(){
            setTimeout(function(){
                TravlCarousel();
            }, 1000);
        });
    </script>
@endsection

