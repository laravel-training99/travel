@extends('layouts.main')
@section('title', $title)
@section('styles')
    <link rel="stylesheet" href="{{asset('main')}}/vendor/toastr/css/toastr.min.css">
@endsection
@section('contents')
    <div class="d-flex justify-content-between align-items-center flex-wrap">
        <div class="card-action coin-tabs mb-3">

        </div>
        <div class="d-flex align-items-center mb-3">
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="tab-content">
                        <div class="tab-pane active show" >
                            <div class="table-responsive">
                                <table class="table card-table display mb-4 shadow-hover default-table table-responsive-lg dataTable no-footer" id="guestTable-all">
                                    <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Amount</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>User</th>
                                        <th>Booking Code</th>
                                        <th>Created at</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transactions as $value)
                                        <tr>
                                            <td class="fw-bold">
                                                #{{$value->code}}
                                            </td>
                                            <td >
                                                {{number_format($value->amount)}}
                                            </td>
                                            <td>
                                                {{$value->type}}
                                            </td>
                                            <td class="text-nowrap">
                                                @if($value->status == VNP_SUCCESS)
                                                    <span class="text-success">SUCCESS</span>
                                                @else
                                                    <span class="text-danger">ERROR</span>
                                                @endif
                                            </td>
                                            <td>{{$value->user->name}}<br>{{$value->user->email}}<br>{{$value->user->phone}}</td>
                                            <td>#{{$value->booking_code}}</td>
                                            <td>{{timeAgo($value->created_at)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-between">
                                    <div class="dataTables_info" id="guestTable-all_info" role="status" aria-live="polite"></div>
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        <nav>
                                            @include('main.component.pagination',['datas' => $transactions])
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('main')}}/vendor/toastr/js/toastr.min.js"></script>
    <script src="{{asset('main')}}/js/plugins-init/toastr-init.js"></script>
    <script type="text/javascript">
        let message = '{{ session('message') }}'
        let status = '{{ session('status') }}'
        if(message) {
            if(status){
                toastr.success(message)
            }else {
                toastr.error(message)
            }
        }
    </script>
@endsection

