@extends('layouts.main')
@section('title', $title)
@section('styles')
    <link rel="stylesheet" href="{{asset('main')}}/vendor/toastr/css/toastr.min.css">
@endsection
@section('contents')
    <div class="row mt-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="tab-content">
                        <div class="tab-pane active show" >
                            <div class="table-responsive">
                                <table class="table card-table display mb-4 shadow-hover default-table table-responsive-lg dataTable no-footer" id="guestTable-all">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Location</th>
                                        <th>Address</th>
                                        <th>Created at</th>
                                        <th class="bg-none">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($assets as $value)
                                    <tr>
                                        <td>
                                            <div class="room-list-bx d-flex align-items-center">
                                                <img class="me-3 rounded" src="{{$value->image[0]->image_path}}" alt="" style="object-fit: cover">
                                                <div>
                                                    <span class=" fs-16 font-w500 text-nowrap">{{$value->name}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <span class="font-w600 text-nowrap">
                                                    <i class="flaticon-381-location me-2"></i>
                                                    {{$value->province}}
                                                </span>
                                            </div>
                                        </td>
                                        <td class="job-desk3">
                                            <p>
                                                {{$value->address}}, {{$value->district}}, {{$value->province}}
                                            </p>
                                        </td>
                                        <td>
                                            <span class="font-w600 text-nowrap">{{timeAgo($value->created_at)}}</span>
                                        </td>
                                        <td class="text-nowrap">
                                            <form action="{{ route('assets.destroy',$value->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                @if(checkRoom($value->id))
                                                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                @endif
                                                <a href="{{route('assets.edit', $value->id)}}" class="btn btn-sm btn-warning">Update</a>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-between">
                                    <div class="dataTables_info" id="guestTable-all_info" role="status" aria-live="polite"></div>
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        <nav>
                                            @include('main.component.pagination',['datas' => $assets])
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('main')}}/vendor/toastr/js/toastr.min.js"></script>
    <script src="{{asset('main')}}/js/plugins-init/toastr-init.js"></script>
    <script type="text/javascript">
        let message = '{{ session('message') }}'
        let status = '{{ session('status') }}'
        if(message) {
            if(status){
                toastr.success(message)
            }else {
                toastr.error(message)
            }
        }
    </script>
@endsection

