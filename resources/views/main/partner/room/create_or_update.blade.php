@extends('layouts.main')
@section('title', $title)
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <style>
        .dropzone .dz-preview .dz-image {
            z-index: 0 !important;
        }
    </style>
@endsection
@section('contents')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{$title}}</h4>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{$action}}" method="post">
                        {{isset($method) ? method_field('PUT') : ""}}
                        @csrf
                        <div class="row">
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.input',
                                    [
                                        'label' => 'Bed',
                                        'id' => "bed",
                                        'placeholder' => "Please enter the bed number!",
                                        'type' => "number",
                                        'name' => "bed",
                                        'value' => $room->bed ?? "",
                                    ]
                                )
                            </div>
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.input',
                                    [
                                        'label' => 'Television',
                                        'id' => "television",
                                        'placeholder' => "Please enter the television number!",
                                        'type' => "number",
                                        'name' => "television",
                                        'value' => $room->television ?? "",
                                    ]
                                )
                            </div>
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.input',
                                    [
                                        'label' => 'Fridge',
                                        'id' => "fridge",
                                        'placeholder' => "Please enter the fridge number!",
                                        'type' => "number",
                                        'name' => "fridge",
                                        'value' => $room->fridge ?? "",
                                    ]
                                )
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.input',
                                    [
                                        'label' => 'Bathroom',
                                        'id' => "bathroom",
                                        'placeholder' => "Please enter the bathroom number!",
                                        'type' => "number",
                                        'name' => "bathroom",
                                        'value' => $room->bathroom ?? "",
                                    ]
                                )
                            </div>
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.input',
                                    [
                                        'label' => 'Maximum number of people',
                                        'id' => "amount_of_people",
                                        'placeholder' => "Please enter the maximum number of people!",
                                        'type' => "number",
                                        'name' => "amount_of_people",
                                        'value' => $room->amount_of_people ?? "",
                                    ]
                                )
                            </div>
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.input',
                                    [
                                        'label' => 'Area',
                                        'id' => "area",
                                        'placeholder' => "Please enter the room size!",
                                        'type' => "number",
                                        'name' => "area",
                                        'value' => $room->area ?? "",
                                    ]
                                )
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-xl-12 col-lg-12">
                                @include('main.component.form.textarea',
                                    [
                                        'label' => 'Description',
                                        'id' => "description",
                                        'placeholder' => "Please enter description room!",
                                        'name' => "description",
                                        'value' => $room->description ?? "",
                                    ]
                                )
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.input',
                                    [
                                        'label' => 'Rate room',
                                        'id' => "price",
                                        'placeholder' => "Please enter room rate!",
                                        'type' => "number",
                                        'name' => "price",
                                        'value' => $room->price ?? "",
                                    ]
                                )
                            </div>
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.select',
                                    [
                                        'label' => 'Property',
                                        'id' => "asset_id",
                                        'name' => "asset_id",
                                        'value' => $room->asset_id ?? "",
                                        'options' => $assets,
                                    ]
                                )
                            </div>
                            <div class="col-xl-4 col-lg-4">
                                @include('main.component.form.select',
                                    [
                                        'label' => 'Breakfast',
                                        'id' => "breakfast",
                                        'name' => "breakfast",
                                        'value' => $room->breakfast ?? "",
                                        'options' => $optionBreakfast,
                                    ]
                                )
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-xl-12 col-lg-12">
                                <div class="dropzone form-control h-auto" id="dropzone">
                                </div>
                            </div>
                            @error('image')
                            <p class="text-danger mt-1">{{ $message }}</p>
                            @enderror
                        </div>
                        <input type="hidden" id="image_link_array" name="image" value="[]">
                        <div class="row mt-4">
                            <div class="col-xl-4 col-lg-4">
                                <button class="btn btn-success bg-primary">Save</button>
                                <a href="{{route('rooms.index')}}" class="btn btn-dark">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        var arrayImage = [];
        var arrayImageRemove = [];
        Dropzone.options.dropzone =
            {
                maxFilesize: 12,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time+file.name;
                },
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: 'post',
                url: '{{ route('upload.image') }}',
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                timeout: 2000,
                init: function() {
                    myDropzone = this;
                    if('{{isset($method)}}'){
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                            },
                            url: '{{route('update.image.data')}}',
                            type: 'post',
                            data: {
                              'id' : '{{request()->url()}}'.replace(/[^0-9]/g,'').slice(-1)
                            },
                            dataType: 'json',
                            success: function(response){
                                console.log(response)
                                $.each(response, function(key,value) {
                                    var mockFile = { name: value.image_path, size: 12345};
                                    myDropzone.emit("addedfile", mockFile);
                                    myDropzone.emit("thumbnail", mockFile, '{{asset('images')}}/' + value.image_path);
                                    myDropzone.emit("complete", mockFile)
                                    $('.dz-image img').css({"width":"100%"});
                                });
                            }
                        });
                    }
                },
                removedfile: function(file)
                {
                    if(file.upload){
                        var name = file.upload.filename;
                    }else {
                        var name = file.name;
                    }
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        type: 'post',
                        url: '{{ route('delete.image') }}',
                        data: {filename: name},
                        success: function (image_id){
                            arrayImageRemove.push(image_id);
                            const newArray = arrayImage.filter((value) => {
                                return !arrayImageRemove.includes(value)
                            })
                            $("#image_link_array").val(JSON.stringify(newArray));
                        },
                        error: function(e) {
                            console.log(e);
                        }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ?
                        fileRef.parentNode.removeChild(file.previewElement) : void 0;
                },
                success: function(file, response)
                {
                    arrayImage.push(response);
                    $("#image_link_array").val(JSON.stringify(arrayImage));
                }
            };
    </script>

@endsection
