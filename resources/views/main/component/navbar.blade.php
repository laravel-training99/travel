<div class="dlabnav">
    <div class="dlabnav-scroll">
        <ul class="metismenu" id="menu">
            @if(auth()->user()->role == ADMIN)
            <li>
                <a href="{{route('admin.dashboard')}}" aria-expanded="false">
                    <i class="flaticon-025-dashboard"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.client.list')}}" aria-expanded="false">
                    <i class="la la-users"></i>
                    <span class="nav-text">Clients</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.partner.list')}}" aria-expanded="false">
                    <i class="flaticon-381-user"></i>
                    <span class="nav-text">Partners</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.asset.list')}}" aria-expanded="false">
                    <i class="flaticon-381-home"></i>
                    <span class="nav-text">Assets</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.room.list')}}" aria-expanded="false">
                    <i class="flaticon-381-home-1"></i>
                    <span class="nav-text">Rooms</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.bookings.index')}}" aria-expanded="false">
                    <i class="icon-Ticket" style="font-family: 'icomoon' !important;"></i>
                    <span class="nav-text">Booking</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.transactions.index')}}" aria-expanded="false">
                    <i class="icon-Ticket" style="font-family: 'icomoon' !important;"></i>
                    <span class="nav-text">Payment</span>
                </a>
            </li>
            @else
                <li>
                    <a href="{{route('partner.dashboard')}}" aria-expanded="false">
                        <i class="flaticon-025-dashboard"></i>
                        <span class="nav-text">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a class="has-arrow " href="javascript:void()" aria-expanded="false">
                        <i class="flaticon-381-home"></i>
                        <span class="nav-text">Assets</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{route('assets.index')}}">List</a></li>
                        <li><a href="{{route('assets.create')}}">Create</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow " href="javascript:void()" aria-expanded="false">
                        <i class="flaticon-381-home-1"></i>
                        <span class="nav-text">Rooms</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{route('rooms.index')}}">List</a></li>
                        <li><a href="{{route('rooms.create')}}">Create</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('bookings.index')}}" aria-expanded="false">
                        <i class="icon-Ticket" style="font-family: 'icomoon' !important;"></i>
                        <span class="nav-text">Booking</span>
                    </a>
                </li>
            @endif

        </ul>
        <div class="dropdown header-profile2 ">
            <div class="header-info2 text-center">
                <img src="{{auth()->user()->image}}" alt=""/>
                <div class="sidebar-info">
                    <div>
                        <h5 class="font-w500 mb-0">{{auth()->user()->name}}</h5>
                        <span class="fs-12">{{auth()->user()->email}}</span>
                    </div>
                </div>
                <div>
                    <a href="{{route('logout')}}" class="btn btn-md text-secondary">Logout</a>
                </div>
            </div>
        </div>
        <div class="copyright">
            <p class="text-center"><strong>Travl Hotel Admin Dashboard</strong> © 2021 All Rights Reserved</p>
            <p class="fs-12 text-center">Made with <span class="heart"></span> by DexignLab</p>
        </div>
    </div>
</div>
