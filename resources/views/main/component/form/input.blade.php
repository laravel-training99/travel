<div class="mb-3">
    <label class="fs-16 ms-1" for="{{$id}}">
        {{$label}}
        <span class="text-danger">*</span>
    </label>
    <input class="form-control input-default "
           placeholder="{{$placeholder ?? ""}}"
           type="{{$type}}"
           name="{{$name}}"
           id="{{$id}}"
           value="{{ old($name) ? old($name) : $value ?? ""}}"
    >
    @error($name)
    <p class="text-danger mt-1">{{ $message }}</p>
    @enderror
</div>
