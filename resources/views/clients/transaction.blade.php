@extends('layouts.client')
@section('title', $title)
@section('styles')
    <style>
        .pagination {
            display: flex;
            padding-left: 0;
            list-style: none;
        }
        .page-link {
            position: relative;
            display: block;
            color: #029e9d;
            background-color: #fff;
            border: 1px solid #dee2e6;
            border-radius: 5px;
            margin-right: 5px;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        .page-link:hover {
            z-index: 2;
            color: #515acc;
            background-color: #e9ecef;
            border-color: #dee2e6;
        }
        .page-link:focus {
            z-index: 3;
            color: #515acc;
            background-color: #e9ecef;
            outline: 0;
            box-shadow: none;
        }
        .page-item.active .page-link,
        .page-item:hover .page-link {
            z-index: 3;
            color: #fff;
            background-color: #029e9d;
            border-color: #029e9d;
        }
        .page-link {
            padding: 0.469rem 1rem;
        }
        .pagination-lg .page-link {
            padding: 0.5rem 1.1rem;
            font-size: 1rem;
        }
        .pagination-sm .page-link {
            padding: 0.391rem 0.75rem;
            font-size: 0.812rem;
        }
        .pagination .page-item .page-link svg {
            width: 18px;
            height: 18px;
        }
        .pagination.pagination-separated .page-item {
            margin-left: 2px;
            margin-right: 2px;
        }
        .pagination.pagination-separated .page-item:first-child {
            margin-left: 0;
        }
        .pagination.pagination-separated .page-item:last-child {
            margin-right: 0;
        }
        .pagination.pagination-rounded .page-item {
            margin-right: 2px;
            margin-left: 2px;
        }
        .pagination.pagination-rounded .page-item .page-link {
            border-radius: 50px;
        }
        .pagination {
            justify-content: center;
        }
    </style>
@endsection
@section('contents')
    <section class="breadcrumb-main pb-20 pt-14" style="background-image: url(http://travel.test/client/images/bg/bg1.jpg);">
        <div class="section-shape section-shape1 top-inherit bottom-0" style="background-image: url(http://travel.test/client/images/shape8.png);"></div>
        <div class="breadcrumb-outer">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h1 class="mb-3">List Transaction</h1>
                    <nav aria-label="breadcrumb" class="d-block">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">List Transaction</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="dot-overlay"></div>
    </section>
    <div class="container">
        <form class="forms-sample pt-6 mb-4" method="get" action="{{route('client.transactions.list')}}">
            <div class="row align-items-center">
                <div class="col-lg-1">
                    <h5>Filter Transaction</h5>
                </div>
                <div class="col-lg-4 col-md-4">
                    <input type="text" placeholder="Search by list transaction" name="keyword" value="{{Request::get('keyword') ? Request::get('keyword') : ''}}" class="form-control">
                </div>
                <div class="col-lg-1">
                    <button class="nir-btn w-100">Filter</button>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTableExample" class="table">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Booking code</th>
                                    <th>PAYMENT</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <td>#{{$transaction->code}}</td>
                                        <td>{{number_format($transaction->amount)}} đ</td>
                                        <td>{{$transaction->type}}</td>
                                        <td>#{{$transaction->booking_code}}</td>
                                        <td>
                                            @if($transaction->status == VNP_SUCCESS)
                                                <span class="white bg-theme p-1 px-2 rounded">SUCCESS</span>
                                            @else
                                                <span class="white bg-danger p-1 px-2 rounded">ERROR</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="dataTables_paginate">
                @include('clients.components.pagination', ['datas' => $transactions])
            </div>
        </div>
    </div>
@endsection
