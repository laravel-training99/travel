<header class="main_header_area">

    <div class="header_menu" id="header_menu">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-flex d-flex align-items-center justify-content-between w-100 pb-3 pt-3">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="/">
                            <img src="{{asset('client')}}/images/logo.png" alt="image" />
                        </a>
                    </div>

                    <div class="navbar-collapse1 d-flex align-items-center" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav" id="responsive-menu">
                            <li class="{{request()->is('/') ? 'active' : '' }}">
                                <a href="/">Home</a>
                            </li>
                            <li class="{{request()->is('booking') ? 'active' : '' }}">
                                <a href="{{route('client.booking')}}">Booking</a>
                            </li>
                            @if(auth()->check())
                                <li class="{{request()->is('booking-lists') ? 'active' : '' }}">
                                    <a href="{{route('client.booking.list')}}">List Booking</a>
                                </li>
                                <li class="{{request()->is('transactions') ? 'active' : '' }}">
                                    <a href="{{route('client.transactions.list')}}">Transaction</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="register-login d-flex align-items-center">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal" class="me-3">
                            @if(auth()->check())
                                <a href="{{route('logout')}}" class="me-2">
                                    <img src="{{auth()->user()->image}}" style="width: 30px;height: 30px;border-radius: 33px;" alt="">
                                    {{auth()->user()->user_name}}
                                </a>
                                @if(auth()->user()->role == ADMIN)
                                    <a href="{{route('admin.dashboard')}}" class="nir-btn white">Dashboard</a>
                                @endif
                                @if(auth()->user()->role == PARTNER)
                                    <a href="{{route('partner.dashboard')}}" class="nir-btn white">Dashboard</a>
                                @endif
                            @else
                                <i class="icon-user"></i> Login/Register
                                <a href="{{route('client.booking')}}" class="nir-btn white">Book Now</a>
                            @endif
                        </a>
                    </div>
                    <div id="slicknav-mobile"></div>
                </div>
            </div>
        </nav>
    </div>
</header>
