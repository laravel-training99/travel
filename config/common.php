<?php
return [
    'breakfast' => [
        '0' => [
            'id' => 0,
            'name' => 'No breakfast'
        ],
        '1' => [
            'id' => 1,
            'name' => 'Breakfast (included in the package)'
        ],
        '2' => [
            'id' => 2,
            'name' => 'Breakfast included (not included in the package)'
        ]
    ],

    'status_class' => [
        '1' => 'badge badge-warning',
        '2' => 'badge badge-success',
        '3' => 'badge badge-danger',
    ],


];
