<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $limit = 10;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('assets')->insert([
                'name' => $faker->name,
                'province' => "demo",
                'district' => $faker->city,
                'address' => $faker->address(),
                'user_id' => $faker->numberBetween(1,20)
            ]);
        }
    }
}
